#!/bin/bash

PROCESS=${@}

if [[ -z $PROCESS ]]; then
	echo "Enter valid process name"
else
	for p in $PROCESS; do
		RAMUSAGE=$(ps -ely | awk -v process=$p '$13 == process' | awk '{SUM += $8/1024} END {print SUM}' | cut -d "." -f1)
		if [[ -z $RAMUSAGE ]]; then
			echo "No process $p"
		else
			echo "$p : $RAMUSAGE MB"
		fi

	done
fi
